public class ParImpar {
    public void parOuImpar(int numero) {
        if (numero % 2 == 0) {
            System.out.printf("O número %d é par\n", numero);
        } else {
            System.out.printf("O número %d é ímpar\n", numero);
        }
    }
}
