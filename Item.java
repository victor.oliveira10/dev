public class Item {
    private String title;
    private String publisher;
    private int yearPublished;
    private String isbn;
    private float price;

    public Item(String title, String publisher, int yearPublished, String isbn, float price) {
        this.title = title;
        this.publisher = publisher;
        this.yearPublished = yearPublished;
        this.isbn = isbn;
        this.price = price;
    }

    public void display() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "title='" + title + '\'' +
                ", publisher='" + publisher + '\'' +
                ", yearPublished=" + yearPublished +
                ", isbn='" + isbn + '\'' +
                ", price=" + price;
    }
}
