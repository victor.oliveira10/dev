public class Book extends Item {
    private String author;
    private int edition;
    private int volume;

    public Book(String title, String publisher, int yearPublished, String isbn, float price, String author, int edition, int volume) {
        super(title, publisher, yearPublished, isbn, price);
        this.author = author;
        this.edition = edition;
        this.volume = volume;
    }

    @Override
    public void display() {
        System.out.println(this + super.toString());
    }

    @Override
    public String toString() {
        return "author='" + author + '\'' +
                ", edition=" + edition +
                ", volume=" + volume;
    }
}
